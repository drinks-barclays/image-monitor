﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Amazon.CloudFront;
using Amazon.S3;
using Amazon;
using Amazon.CloudFront.Model;
using Amazon.S3.Model;

namespace WebsiteImageMonitor
{

    public class Logger
    {
        private static StreamWriter logFileStream;
        private static string fileName = String.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, "log.txt");
        
        public Logger()
        {
            
        }

        public static void LogEvent(string Message, EventLogEntryType EventType = EventLogEntryType.Information)
        {
            // No try/catch here, let the application write to the event log.
            logFileStream = new StreamWriter(fileName, true);
            logFileStream.WriteLine(String.Format("{0} {1}", DateTime.Now.ToString(), Message));
            logFileStream.Flush();
            logFileStream.Close();
        }
    }

    public class FileSystemWatcherForS3 : FileSystemWatcher
    {
        // The name of the S3 bucket the files will be written to. The S3 folder is user specified (through the config)
        private string bucket = ConfigurationManager.AppSettings["Bucket"];
        private RegionEndpoint endpoint = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["AWSRegion"]);
        private string distributionId = ConfigurationManager.AppSettings["DistributionId"];

        public string S3Folder { get; set; }

        public FileSystemWatcherForS3(string Path, string S3Folder) : base(Path)
        {
            // Initialize some values regarding watching subdirectories and setting the events to watch
            this.IncludeSubdirectories = true;
            this.S3Folder = S3Folder;
            this.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.LastWrite;
            this.Created += WebSiteFileSystemWatcher_CreatedChanged;
            this.Changed += WebSiteFileSystemWatcher_CreatedChanged;
            this.Deleted += FileSystemWatcherForS3_Deleted;
            this.EnableRaisingEvents = true;
        }

        private void FileSystemWatcherForS3_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                AmazonS3Client client = new AmazonS3Client(endpoint);
                string relativePath = e.FullPath.Replace(this.Path, "").Replace(@"\", "/").Substring(1);
                string awsKey = String.Format(@"{0}", this.S3Folder + relativePath);
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest
                {
                    BucketName = bucket,
                    Key = awsKey
                };
                client.DeleteObject(deleteRequest);
            }
            catch (Exception ex)
            {
                Logger.LogEvent(String.Format("{0}\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private void WebSiteFileSystemWatcher_CreatedChanged(object sender, FileSystemEventArgs e)
        {
            // Sometimes when a file is being copied, a file with the extension ".filepart" temporarily appears in the folder.
            // The FileSystemWatcher grabs this to process, and then complains when the file disappears. The line of code below
            // addresses that.
            if (e.FullPath.EndsWith("filepart")) return;
            Dictionary<string, string> contentTypes = new Dictionary<string, string>();
            contentTypes.Add("jpg", "image/jpeg");
            contentTypes.Add("png", "image/png");
            contentTypes.Add("text", "text/plain");
            contentTypes.Add("gif", "image/gif");

            try
            {
                // Sometimes the FileSystemWatcher gets a little trigger happy and fires this event before the file has been fully copied.
                // To remedy this we'll put the thread to sleep for some time.
                int sleepPeriod;
                Int32.TryParse(ConfigurationManager.AppSettings["SleepPeriod"], out sleepPeriod);
                if (sleepPeriod == 0) sleepPeriod = 4500;
                System.Threading.Thread.Sleep(sleepPeriod);
                
                // For some reason the FileSystemWatcher fires a change event for the directory as well as the file.
                // The code below ignores the directory change event.
                DirectoryInfo di = new DirectoryInfo(e.FullPath);
                if (di.Exists) return;

                // Strip the root level path to get the relative path. Also switch the back slashes (Windows) to forward slashes (AWS/UNIX)
                string relativePath = e.FullPath.Replace(this.Path, "").Replace(@"\", "/").Substring(1);

                string extension = "";
                string[] fileParts = e.Name.Split(new char[] { '.' });
                if (fileParts.Length >= 1) extension = fileParts[fileParts.Length - 1];

                // Upload the file to AWS
                AmazonS3Client client = new AmazonS3Client(endpoint);
                string awsKey = String.Format(@"{0}", this.S3Folder + relativePath);

                // See if an object with the key already exists in S3/Cloudfront. If so, we'll have to invalidate it.
                var listResponse = client.ListObjects(new Amazon.S3.Model.ListObjectsRequest
                {
                    BucketName = bucket,
                    Prefix = awsKey
                });

                // Construct the request to upload the object
                var putObjectRequest =  new Amazon.S3.Model.PutObjectRequest
                {
                    FilePath = e.FullPath,
                    BucketName = bucket,
                    Key = awsKey,
                    CannedACL = S3CannedACL.PublicRead
                };

                // Set the object to expire from the cache in 24 hours
                putObjectRequest.Metadata.Add("Cache-Control", "max-age=86400");

                // Set the content type if we know what it is
                var contentType = (from i in contentTypes
                                   where i.Key == extension
                                   select i.Value).FirstOrDefault();
                if (contentType != null) putObjectRequest.ContentType = contentType;

                // Submit the PUT request to the server
                var response = client.PutObject(putObjectRequest);


                // If there is a valid get object response then the record needs to be invalidated
                if (listResponse.S3Objects.Count > 0)
                {
                    // MJ - turning this off until we can do more testing and avoid unnecessary invalidation creations.
                    // For now we'll do this manually when required.
                    return;
                    AmazonCloudFrontClient cloudFrontClient = new AmazonCloudFrontClient(endpoint);
                    InvalidationBatch invalidationBatch = new InvalidationBatch { CallerReference = Guid.NewGuid().ToString() };
                    invalidationBatch.Paths = new Paths();
                    invalidationBatch.Paths.Items.Add(String.Format("/{0}", awsKey));
                    invalidationBatch.Paths.Quantity = 1;
                    var invalidationResponse = cloudFrontClient.CreateInvalidation(new CreateInvalidationRequest
                    {
                        DistributionId = distributionId,
                        InvalidationBatch = invalidationBatch
                    });
                }
                
            }
            catch (Exception ex)
            {
                Logger.LogEvent(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
            }
        }
    }

    public partial class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.LogEvent("Starting the file monitoring service...");
                // Load the document that holds the sites
                XDocument doc = XDocument.Load(String.Format(@"{0}\Sites.xml", AppDomain.CurrentDomain.BaseDirectory));

                // Create a FileSystemWatcher instance for each folder to watch
                var sites = from i in doc.Descendants("site") select new { LocalFolder = i.Attribute("localFolder").Value, S3Folder = i.Attribute("s3Folder").Value };
                
                foreach (var site in sites)
                {
                    FileSystemWatcherForS3 fsw = new FileSystemWatcherForS3(site.LocalFolder, site.S3Folder);
                    Logger.LogEvent(String.Format("Monitoring folder {0} to copy files to {1}.", site.LocalFolder, site.S3Folder));
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(String.Format("{0}\n{1}", ex.Message, ex.StackTrace));
            }
        }

        
        protected override void OnStop()
        {
        }
    }
}
